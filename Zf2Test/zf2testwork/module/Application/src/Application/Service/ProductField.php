<?php

namespace Application\Service;

class ProductField
{
    public function getFields()
    {
        $fieldsConfigArray = array(
            array(
                'fieldName'=>'id',
                'label'=>'',
                'visible'=>0,
                'sorting'=>0,
                'searching'=>0,
            ),
            array(
                'fieldName'=>'product_name',
                'label'=>'Product name',
                'visible'=>1,
                'sorting'=>1,
                'searching'=>1,
            ),
            array(
                'fieldName'=>'image_name',
                'label'=>'Product image',
                'visible'=>1,
                'sorting'=>0,
                'searching'=>0,
            ),
            array(
                'fieldName'=>'category_name',
                'label'=>'Category name',
                'visible'=>1,
                'sorting'=>1,
                'searching'=>0,
            ),
            array(
                'fieldName'=>'created_date',
                'label'=>'Created date',
                'visible'=>1,
                'sorting'=>1,
                'searching'=>0,
            ),
        );
        return $fieldsConfigArray;
    }
}
<?php
/**
 * methods used to apply on the product master table
 */

namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Cache\Storage\StorageInterface;


class ProductMasterTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchData( $optionArray = [], $pagination = false )
    {
        unset($optionArray['fieldArray'][3]);
        if ($pagination)
        {
            $select = new Select('product_master');
            if(!empty($optionArray['fieldArray']))
            {
                $select->columns($optionArray['fieldArray']);    
            }
            $select ->join('category_master', 'category_master.id = product_master.category_id', array('category_name'), $select::JOIN_LEFT);
            if(!empty($optionArray['sortByColumn']['sort_column']) && !empty($optionArray['sortByColumn']['sort_order']))
            {
                $orderBy=$optionArray['sortByColumn']['sort_column'].' '.$optionArray['sortByColumn']['sort_order'];
                $select->order($orderBy);    
            }
            else
            {
                if(!empty($optionArray['default_sort_column']) && !empty($optionArray['default_sort_order']))
                {
                    $orderBy=$optionArray['default_sort_column'].' '.$optionArray['default_sort_order'];
                    $select->order($orderBy);    
                }
            }
            if(!empty($optionArray['whereCol']) && !empty($optionArray['whereKey']))
            {
                foreach($optionArray['whereCol'] as $key=>$value)
                {
                    $where->equalTo($optionArray['whereCol'][$key],$optionArray['whereKey'][$key]);
                    
                }
                $select->where($where);
            }
            if(!empty($optionArray['searchColumns']['searchKey']) && !empty($optionArray['searchColumns']['searchCol']))
            {
                $searchKey="%".str_replace("_","\_",$optionArray['searchColumns']['searchKey'])."%";
                foreach($optionArray['searchColumns']['searchCol'] as $key=>$val)
                {
                    $searchCol=($val?$val:$optionArray['fieldArray'][1]);
                    
                    if($key>0)
                    {
                        if($key + 1 == sizeof($optionArray['searchColumns']['searchCol']))
                        {
                            $query .= " OR ".$searchCol." LIKE '".$searchKey."' )";
                        }
                        else
                        {
                            $query .= " OR ".$searchCol." LIKE '".$searchKey."'";
                        }
                       
                    }
                    else
                    {
                        if($key + 1 == sizeof($optionArray['searchColumns']['searchCol']))
                        {
                            $query .= " (".$searchCol." LIKE '".$searchKey."' )";
                        }
                        else
                        {
                            $query .= "(".$searchCol." LIKE '".$searchKey."'";
                        }
                    }
                }
                $select->where($query);
            }
               
                //$sql = $this->tableGateway->getSql();
                //echo $sql->getSqlstringForSqlObject($select);die;
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ProductMaster());
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function addData( $dataArray )
    {
        $this->tableGateway->insert( $dataArray );
        return $this->tableGateway->lastInsertValue;
    }
    
    public function updateData( $dataArray, $whereArray )
    {
        $this->tableGateway->update( $dataArray, $whereArray );
        return $this->tableGateway->lastInsertValue;
    }
    
    public function getSingleData( $whereArray )
    {
        $data = $this->tableGateway->select( $whereArray );
        
        return $data->current();
    }
    
    public function deleteProduct( $whereArray )
    {
        $this->tableGateway->delete( $whereArray );
    }
}

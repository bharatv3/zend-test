<?php
/**
 * product master attributes
 */

namespace Application\Model;

class ProductMaster
{
    public $id;
    public $product_name;
    public $product_description;
    public $category_id;
    public $image_name;
    public $created_date;
    public $modified_date;
    public $category_name;
    
    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->product_name = (!empty($data['product_name'])) ? $data['product_name'] : null;
        $this->product_description = (!empty($data['product_description'])) ? $data['product_description'] : null;
        $this->category_id = (!empty($data['category_id'])) ? $data['category_id'] : null;
        $this->image_name = (!empty($data['image_name'])) ? $data['image_name'] : null;
        $this->created_date = (!empty($data['created_date'])) ? $data['created_date'] : null;
        $this->modified_date = (!empty($data['modified_date'])) ? $data['modified_date'] : null;
        $this->category_name = (!empty($data['category_name'])) ? $data['category_name'] : null;
    }
}

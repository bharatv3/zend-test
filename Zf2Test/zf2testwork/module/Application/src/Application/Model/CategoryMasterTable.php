<?php
/**
 * methods used to apply on the category master table
 */

namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class CategoryMasterTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll( $whereArray = [] )
    {
        return $this->tableGateway->select( $whereArray );
    }
    
    public function fetchJoin( $whereArray = [] )
    {
        $resultSet = $this->tableGateway->select(function(Select $select) {
            $select->join('product_master', 'product_master.category_id = category_master.id', array('product_name'));
            //$sql = $this->tableGateway->getSql();
            //echo $sql->getSqlstringForSqlObject($select);die;
        });
        return $resultSet;
    }
}

<?php
/**
 * category master attributes
 */

namespace Application\Model;

class CategoryMaster
{
    public $id;
    public $category_name;
    public $product_name;
    
    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->category_name = (!empty($data['category_name'])) ? $data['category_name'] : null;
        $this->product_name = (!empty($data['product_name'])) ? $data['product_name'] : null;
    }
}

<?php
/**
 * 
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\ProductForm;
use Application\Classes\ProductModel;
use Application\Classes\CategoryModel;
use Application\Service\ProductField;
use Application\Classes\FileProcess;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    
    public function productAction()
    {
        $messages = '';
        $errorMessages = '';
        if( $this->flashMessenger()->hasMessages())
        {
            $messages = $this->flashMessenger()->getMessages();
        }
        elseif( $this->flashMessenger()->hasErrorMessages())
        {
            $errorMessages = $this->flashMessenger()->getErrorMessages();
        }
        $categoryObj = new CategoryModel( $this );
        $categories = $categoryObj->fetchAllCategory();
        $request = $this->getRequest();
        $params = $request->getQuery();
        $optionArray = array();
        if(!empty($params['sort']) && !empty($params['order']))
        {
            $optionArray['sortByColumn']['sort_column'] = $params['sort'];
            $searchCol = $optionArray['sortByColumn']['sort_column'] ;
            $optionArray['sortByColumn']['sort_order'] = $params['order'];
            $searchOrder = $optionArray['sortByColumn']['sort_order'];
        }
        if(!empty($params['search']))
        {
            $optionArray['searchColumns']['searchKey'] = $params['search'];
            $searchKey = $optionArray['searchColumns']['searchKey'] ;
                
        }
        if(!empty($params['category']))
        {
            $optionArray['whereCol'][] = 'category_id';
	    $optionArray['whereKey'][] = $params['category'];
        }
        $productFieldObj = new ProductField();
        $fields = $productFieldObj->getFields();
                
        foreach($fields as $field)
        {
            $optionArray['fieldArray'][] = $field['fieldName'];
            if($field['searching'] == 1)
            {
                $optionArray['searchColumns']['searchCol'][] = $field['fieldName'];
            }
        }
        $optionArray['default_sort_column'] = 'created_date';
        $optionArray['default_sort_order'] = 'DESC';
        $productObj = new ProductModel( $this );
        $paginator = $productObj->fetchPaginatedData( $optionArray, true );
        $page = (int)$this->params()->fromQuery('page', 1);
        $paginator->setCurrentPageNumber($page);
        $serialNumber = ($page-1)*10+1;
        $paginator->setItemCountPerPage(10);
        return new ViewModel([
            'categories'=>$categories,
            'paginator' => $paginator,
            'messages' => $messages,
            'errorMessages' => $errorMessages,
            'fields' => $fields,
            'showSearch' => 1,
            'defaultSortOrder' => (($params['order'] == 'ASC' || empty($params['order']))?'DESC':'ASC'),
            'serialNumber' => $serialNumber,
            'searchKey' => $searchKey,
            'searchCol' => $searchCol,
            'searchOrder' => $searchOrder,
            'page' => $page,
            'category_name'=>$params['category']
        ]);
    }
    
    public function addProductAction()
    {
        $categoryObj = new CategoryModel( $this );
        $productObj = new ProductModel( $this );
        $categories = $categoryObj->fetchAllCategory();
        
        $request = $this->getRequest();
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        $routeName = $this->getEvent()->getRouteMatch()->getMatchedRouteName();
        if($routeName == 'edit-product' && $id == '')
        {
            $this->flashMessenger()->addErrorMessage('Something went wrong please select your product to edit again.');
            $this->redirect()->toRoute('product');
        }
        $productData = '';
        
        if($id != '')
        {
            $productData = $productObj->getSingleProductDetail( ['id'=>$id]);
            $formData['selectValue'] = $productData->category_id;
        }
        
        if(empty($productData) && $id != '')
        {
            $this->flashMessenger()->addErrorMessage('The product you looked for doesnot exist.');
            $this->redirect()->toRoute('product');
        }
        $formData['categories'] = $categories;
        
        $form = new ProductForm( $formData );
        
        $translate = $this->getServiceLocator()->get('viewhelpermanager')->get('translate');
        if( $request->isPost() )
        {
            if($request->isPost('image_name'))
            {
                $fileData = $request->getFiles('image_name');			
                $fileUploadObj = new FileProcess();
                $folderName = 'public/images/products/';
                $filesizeAray = '';
                $fileExtensionArray = array('extension' => array('jpeg','jpg','png'));
                $fileNameParameter = 'product_';
                /*function for for upload*/
                $newfile = $fileUploadObj->uploadFile($translate, $fileData, $folderName, $isRequired, $filesizeAray , $fileExtensionArray , $fileNameParameter, '', $skipImage);
            }
            else
            {
                $newfile = '';
            }
            $formData = $form->isMyFormDataValid($request);
            if($formData)
            {
                if($newfile != '')
                {
                    $fileName = explode('\\',$newfile['fileData']);
                    $formData['image_name'] = $fileName[1];
                }
                unset($formData['submit']);
                $time = time();
                $formData['modified_date'] = $time;
                if(!isset($formData['id']))
                {
                    $formData['created_date'] = $time;
                    $productObj->addProductDetails( $formData );
                    
                    $this->flashMessenger()->addMessage('Product added successfully.');
                }
                else
                {
                    if($productData->image_name != NULL)
                    {
                        $filePath = 'public/images/products/'.$productData->image_name;
                        unlink($filePath);
                    }
                    $productObj->updateProductDetails( $formData, ['id'=>$formData['id']] );
                    $this->flashMessenger()->addMessage('Product updated successfully.');
                }
                
                $this->redirect()->toRoute('product');
            }
            else
            {
                if($newfile != '')
                {
                    unlink($newfile['fileData']);
                }
            }
        }
        return new ViewModel( ['form'=>$form, 'formData' => $productData] );
    }
    
    public function deleteProductAction()
    {
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        $productObj = new ProductModel( $this );
        if($id != '')
        {
            $productData = $productObj->getSingleProductDetail( ['id'=>$id] );
        }
        $request = $this->getRequest();
        if ($request->getPost('id')) 
        {
            $id = $request->getPost('id');
            $productData = $productObj->getSingleProductDetail( ['id'=>$id] );
            
            $productObj->deleteProduct( ['id'=>$id] );
            if($productData->image_name != NULL)
            {
                $filePath = 'public/images/products/'.$productData->image_name;
                unlink($filePath);
            }
            $this->flashMessenger()->addMessage('Product deleted successfully.');
            echo '1';
            die;
        }
        $viewModel = new ViewModel();
	return $viewModel->setVariables(array('productName' => $productData->product_name, 'id' => $id))->setTerminal(true);
    }
    
    public function createCategoryCacheAction()
    {
        $categoryObj = new CategoryModel( $this );
        $categories = $categoryObj->fetchCategoryJoin();
        $this->getServiceLocator()->get('cache')->setItem('categoryArray', $categories);
        print_r($this->getServiceLocator()->get('cache')->getItem('categoryArray'));
        $this->flashMessenger()->addMessage('Cache created successfully.');
        $this->redirect()->toRoute('product');
    }
    
    public function clearCacheAction()
    {
        $this->getServiceLocator()->get('cache')->removeItem('categoryArray');
        $this->flashMessenger()->addMessage('Cache cleared successfully.');
        $this->redirect()->toRoute('product');
    }
    
    public function showCacheAction()
    {
        if($this->getServiceLocator()->get('cache')->hasItem('categoryArray'))
        {
            echo '<pre>';
            print_r($this->getServiceLocator()->get('cache')->getItem('categoryArray'));
            echo '</pre>';
        }
        else
        {
            echo 'Cache is clear';
        }
        die;
    }
    
    public function deleteMultiProductAction()
    {
        $productObj = new ProductModel( $this );
        $request = $this->getRequest();
        if ($request->getPost('multi')) 
        {
            $id = $request->getPost('multi');
            $multiIds = explode(',',$id);
            foreach($multiIds as $value)
            {
                $productData = $productObj->getSingleProductDetail( ['id'=>$value] );
                $productObj->deleteProduct( ['id'=>$value] );
                if($productData->image_name != NULL)
                {
                    $filePath = 'public/images/products/'.$productData->image_name;
                    unlink($filePath);
                }
            }
            $this->flashMessenger()->addMessage('Products deleted successfully.');
            echo '1';
            die;
        }
        $viewModel = new ViewModel();
	return $viewModel->setVariables(array('productName' => $productData->product_name, 'id' => $id))->setTerminal(true);
    }
}

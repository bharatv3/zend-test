<?php

namespace Application\Form;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/*Product form*/
class ProductForm extends Form
{
    public $inputFilter;
    public function __construct( $data = [] )
    {
        parent::__construct();
	$this->inputFilter = new InputFilter();
        /*Form type for product name*/
        $productName = [
            'name' => 'product_name',
            'type' => 'Text',
            'attributes'=>[
                'id'=>'product_name',
                'class'=>'form-control'
            ]
        ];
	$this->add($productName);

        $filterProduct = [
	    'name'     => 'product_name',
	    'filters'  => [
		['name' => 'StripTags'],
		['name' => 'StringTrim']
	    ],
	    'validators'=> [
		
		[
		    'name'    => 'NotEmpty',
		    'break_chain_on_failure' => true,
		    'options' => [
			'message'=>'Please enter product name'
		    ]
		],
		[
		    'name'    => 'StringLength',
		    'break_chain_on_failure' => true,
		    'options' => [
			'min' => 3,
			'max' => 30,
			'message'=>'Please enter minimum %min% and maximum %max% characters for product name'
		    ]
		],
		[
		    'name'    => 'Regex',
		    'break_chain_on_failure' => true,
		    'options' => [
			'pattern' => '/^[a-zA-Z0-9 ]*$/',
			'message'=>'Please enter only digits or number product name'
		    ]
		]
	    ]
	    
        ];
        /* Form type for category selection*/
        $selectCategory = [
            'name' => 'category_id',
            'type' => 'Select',
	    'required'=>false,
            'attributes'=>[
                'id'=>'category_id',
                'class'=>'form-control',
		'value'=>$data['selectValue']
            ]
        ];
        $selectCategory['options']['value_options'] = $data['categories'];
	$this->add($selectCategory);

        $filterCategory = [
	    'name'     => 'category_id',
	    'filters'  => [
		['name' => 'StripTags'],
		['name' => 'StringTrim']
	    ],
	    'validators' => [
		[
		    'name'    => 'NotEmpty',
		    'break_chain_on_failure' => true,
		    'options' => [
			'message'=>'Please select category name'
		    ]
		]
	    ]
        ];
        
        /* Form type for product description*/
        $productDesc = [
            'name' => 'product_description',
            'type' => 'textarea',
            'attributes'=>[
                'id'=>'product_description',
                'class'=>'form-control'
            ]
        ];
	$this->add($productDesc);
	$filterDesc = [
	    'name'     => 'product_description',
	    'filters'  => [
		['name' => 'StripTags'],
		['name' => 'StringTrim']
	    ],
	    'validators' => [
		[
		    'name'    => 'NotEmpty',
		    'break_chain_on_failure' => true,
		    'options' => [
			'message'=>'Please enter description for product'
		    ]
		],
		[
		    'name'     => 'StringLength',
		    'break_chain_on_failure' => true,
		    'options' => [
			'min' => 5,
			'message'=>'Please enter minimum %min% characters.'
		    ]
		]
	    ]
        ];
        
        /* Form type for product image*/
        $productImage = [
            'name' => 'image_name',
            'type' => 'file',
            'attributes'=>[
                'id'=>'image_name',
                'class'=>'form-control'
            ]
        ];
	$this->add($productImage);
	
	/* Form type for product hidden id*/
        $hiddenId = [
            'name' => 'id',
            'type' => 'hidden',
            'attributes'=>[
                'id'=>'product_id',
                'class'=>'form-control'
            ]
        ];
	$this->add($hiddenId);
	
	/* Form type for submit*/
        $productImage = [
            'name' => 'submit',
            'type' => 'submit',
            'attributes'=>[
                'id'=>'submit',
                'value'=>'Save'
            ]
        ];
	$this->add($productImage);
	
	$this->inputFilter->add($filterDesc);
	$this->inputFilter->add($filterCategory);
	$this->inputFilter->add($filterProduct);
    }
    
    /*applying the filters prepared above for the form validation*/
    public function isMyFormDataValid( $request )
    {
        $this->setInputFilter($this->inputFilter);
        if($request->isPost())
        {    
            $formData = $request->getPost();
            $this->setData($formData);
            if($this->isValid())
            {
                return $this->getData();
	    
            }
            else
            {
                return false;
            }
        }
    }
}
<?php

namespace Application\Classes;

class ProductModel
{
    public $_controllerObj;
    public $productTable;
    
    public function __construct( $thisVar )
    {
        $this->_controllerObj = $thisVar;
        $sm = $this->_controllerObj->getServiceLocator();
        $this->productTable = $sm->get('Application\Model\ProductMasterTable');
    }
    
    public function fetchPaginatedData( $optionArray = [], $pagination = true)
    {
        return $this->productTable->fetchData( $optionArray, $pagination );
    }
    
    public function addProductDetails( $dataArray )
    {
        return $this->productTable->addData( $dataArray );
    }
    
    public function updateProductDetails( $dataArray, $whereArray )
    {
        return $this->productTable->updateData( $dataArray, $whereArray );
    }
    
    public function getSingleProductDetail( $whereArray )
    {
        return $this->productTable->getSingleData( $whereArray );
    }
    
    public function deleteProduct( $whereArray )
    {
        return $this->productTable->deleteProduct( $whereArray );
    }
}
<?php

namespace Application\Classes;

class CategoryModel
{
    public $_controllerObj;
    public $categoryTable;
    
    public function __construct( $thisVar )
    {
        $this->_controllerObj = $thisVar;
        $sm = $this->_controllerObj->getServiceLocator();
        $this->categoryTable = $sm->get('Application\Model\CategoryMasterTable');
    }
    
    public function fetchAllCategory()
    {
        $categoryData = $this->categoryTable->fetchAll();
        $returnArray = [];
        $returnArray[''] = 'Please select a category';
        foreach( $categoryData as $value)
        {
            $returnArray[$value->id] = $value->category_name;
        }
        return $returnArray;
    }
    
    public function fetchCategoryJoin()
    {
        $categoryData = $this->categoryTable->fetchJoin();
        $returnArray = [];
        foreach( $categoryData as $value)
        {
            $returnArray[$value->category_name][] = $value->product_name;
        }
        return $returnArray;
    }
}
<?php
namespace Application\Classes;
use Zend\Session\Container;
class FileProcess
{
    public function __construct(){
        $this->container = new Container('namespace');
    }
    
    public function uploadFile($translate, $fileData, $folderName, $isRequired = '', $filesizeAray = '', $fileExtensionArray = '', $fileNameParameter = '', $multiUpload = '', $currentFileName = '')
    {
            $httpadapter = new \Zend\File\Transfer\Adapter\Http();
            if($multiUpload != 1)
            {
                if($filesizeAray != '')
                $filesize  = new \Zend\Validator\File\Size($filesizeAray);
                if($fileExtensionArray != '')
                $extension = new \Zend\Validator\File\Extension($fileExtensionArray);
                
                $httpadapter->setValidators(array($extension), $fileData['name']);
                $extFile = explode('.',$fileData['name']);
                if($fileData['name']!='')
                $fileData['target'] = uniqid($fileNameParameter).'.'.$extFile[1];
            }
            $returnArray = [];
            foreach($fileData as $keyData => $valueFile)
            {
                if($valueFile['name'] == '')
                    unset($fileData[$keyData]);
                else
                    $this->container->keyName = $keyData;
            }
            if($httpadapter->isValid()) {
                $httpadapter->setDestination($folderName);
                $httpadapter->addFilter('Rename',$fileData);
                if($httpadapter->receive()) {
                    $newfile = $httpadapter->getFileName();
                    
                    $returnArray['status'] = 'success';
                    if($multiUpload != 1)
                    {
                        $returnArray['fileData'] = $newfile;
                    }
                    else
                    {
                        if(count($fileData) == 1)
                        {
                            $returnArray['fileData'][$fileNameParameter.$this->container->keyName.'_'] = $newfile;
                            unset($this->container->keyName);
                        }
                        else
                        {
                            $returnArray['fileData'] = $newfile;
                        }
                    }
                    $returnArray['dataError'] = '';
                }
                else { die($translate('Error in receiving Image')); }
            }
            else
            {
                $dataError = $httpadapter->getMessages();
                if(isset($dataError['fileUploadErrorNoFile']))
                {
                    $dataError['fileUploadErrorNoFile'] = $translate('Please upload a file.') ;
                }
                elseif(isset($dataError['fileSizeTooSmall']) && $filesizeAray != '')
                {
                    if(isset($filesizeAray['min']))
                    $dataError['fileSizeTooSmall'] = $translate('Minimum size should be '.$filesizeAray['min']) ;
                }
                elseif(isset($dataError['fileSizeTooBig']) && $filesizeAray != '')
                {
                    if(isset($filesizeAray['max']))
                    $dataError['fileSizeTooBig'] = $translate('Maximum size should be '.$filesizeAray['max']) ;
                }
                elseif(isset($dataError['fileUploadErrorIniSize']))
                {
                    $dataError['fileUploadErrorIniSize'] = $translate('Maximum size should be 1MB') ;
                }
                elseif(isset($dataError['fileExtensionFalse']) && $fileExtensionArray != '')
                {
                    $ext = '';
                    foreach($fileExtensionArray['extension'] as $key=>$val)
                    {
                        if($key == 0)
                        {
                            $ext = $val;
                        }
                        else
                        {
                            $ext .= ', '.$val;
                        }
                    }
                    $dataError['fileExtensionFalse'] = $translate('Allowed file extensions are '.$ext) ;
                }
                
                if($isRequired == '' || $fileData['name']=='')
                {
                    unset($dataError['fileUploadErrorNoFile']);
                }
                if(count($dataError)>0)
                {
                    $returnArray['status'] = 'fail';
                    $returnArray['dataError'] = $dataError;
                }
            }
        return $returnArray;
    }
    
     function filesize_r($path){
        if(!file_exists($path)) return 0;
        if(is_file($path)) return filesize($path);
        $ret = 0;
        foreach(glob($path."/*") as $fn)
            $ret += $this->filesize_r($fn);
        return $ret;
    }
}